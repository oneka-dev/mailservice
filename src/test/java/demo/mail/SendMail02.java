package demo.mail;

import java.io.File;
import java.util.Date;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * demo spring boot autowired
 * 
 * @author Rocky
 */
public class SendMail02 {

	@Autowired
	private JavaMailSender javaMailSender;
	
	public static void main(String[] args) throws BeansException, Exception {
		
		ConfigurableApplicationContext ctx = SpringApplication.run(SendMail02.class, "--debug");
		
		ctx.getBean(SendMail02.class).execute("rockywang101@gmail.com", "subject by spring boot");
	}
	
	
	private void execute(String to, String subject) throws Exception {

        MimeMessageHelper helper = new MimeMessageHelper(javaMailSender.createMimeMessage(), true);
//        helper.setFrom(userName, "帥到爆");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText("new test content " + new Date());
        
        helper.addAttachment("testFileName", new File("D:/abc.xls"));
        
        javaMailSender.send(helper.getMimeMessage());
        
        System.out.println("send mail success");
	}
}
