package tw.com.eimi.service.email;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
@SpringBootApplication
public class EmailController {

	private Logger logger = Logger.getLogger(EmailController.class);
	
	@Value("${spring.mail.username}")
	private String username;
	
	@Value("${mail.fromPersonal}")
	private String fromPersonal;
    
    @Autowired
    JavaMailSender javaMailSender;
	
    
	@PostMapping(value="/send")
	public Object sendMailService(	@RequestParam String to, 
									@RequestParam String subject,
									@RequestParam String content,
									@RequestParam(required=false) String cc, 
									@RequestParam(required=false) String bcc,
									@RequestParam(required=false) MultipartFile... files) throws MessagingException, IOException {

		System.out.println("send to => " + to);
		System.out.println("send subject => " + subject);

        MimeMessageHelper helper = null;
        if (files != null && files.length > 0)
        	helper = new MimeMessageHelper(javaMailSender.createMimeMessage(), true);
        else 
        	helper = new MimeMessageHelper(javaMailSender.createMimeMessage());
        
        helper.setFrom(username, fromPersonal);
        helper.setTo(to);
        addCcOrBcc(helper, cc, "cc");
        addCcOrBcc(helper, bcc, "bcc");
        helper.setSubject(subject);
        helper.setText(content, true);
        
        if (files != null) {
        	for (MultipartFile file : files) {
        		System.out.println("attachment file => " + file.getOriginalFilename());
        		helper.addAttachment(file.getOriginalFilename(), file);
        	}
        }
        
        javaMailSender.send(helper.getMimeMessage());
        
        logger.info("send mail success\n");
		return new Date();
	}
	
	
	private void addCcOrBcc(MimeMessageHelper helper, String emails, String action) throws MessagingException {

		if (StringUtils.isBlank(emails))
			return;
		
		System.out.println(action + " to => " + emails);
		// cc or bcc
		boolean isCc = StringUtils.equals(action, "cc");
		
		String[] tokens = null;
		// 以 , 區隔
		if (StringUtils.contains(emails, ","))
			tokens = StringUtils.split(emails, ",");
		// 以 ; 區隔
		else if (StringUtils.contains(emails, ";"))
			tokens = StringUtils.split(emails, ";");
		// Only one email address
		else 
			tokens = new String[] {emails}; 

		for (String email : tokens) {
			if (isCc)
				helper.addCc(email);
			else 
				helper.addBcc(email);
		}
	}
	
	
	
	@GetMapping(value="/send/{to}/{subject}")
	public Object test(@PathVariable final String to, @PathVariable final String subject) throws MessagingException, UnsupportedEncodingException {
		
		System.out.println("send to => " + to);
		System.out.println("send subject => " + subject);
		
		System.out.println("from => " + fromPersonal);

        MimeMessageHelper helper = new MimeMessageHelper(javaMailSender.createMimeMessage(), true);
        
        helper.setFrom(username, fromPersonal);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText("new test content 現在時間： " + new Date());
        
        // TODO
//        helper.addAttachment("testFileName", new File("D:/abc.xls"));
        
        javaMailSender.send(helper.getMimeMessage());
        
        System.out.println("send mail success");
		return new Date();
	}
	
}
