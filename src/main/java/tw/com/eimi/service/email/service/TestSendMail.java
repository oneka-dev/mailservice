package tw.com.eimi.service.email.service;

import java.io.UnsupportedEncodingException;

import org.springframework.core.io.FileSystemResource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


public class TestSendMail {
	
	public static void main(String[] args) throws RestClientException, UnsupportedEncodingException {
		
		RestTemplate restTemplate = new RestTemplate();
		
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		map.add("to", "rockywang101@gmail.com");
		map.add("subject", "中文測試YES");
		map.add("content", "<h2>最後一次測試</h2><h3>Go</h2>");
		map.add("cc", "rocky.wang@eimi.com.tw;eimirockywang@gmail.com");
		// attachment files
		map.add("files", new FileSystemResource("D:/05.jpg"));
		map.add("files", new FileSystemResource("D:/06.png"));
		
		// SSL
		restTemplate.setRequestFactory(SslUtils.getSimpleClientHttpRequestFactory());
		// post
		String result = restTemplate.postForObject("https://uat-www.ponta.com.tw/mailService/send/", map, String.class);
		System.out.println(result);
	}

}
