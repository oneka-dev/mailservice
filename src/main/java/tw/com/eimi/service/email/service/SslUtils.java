package tw.com.eimi.service.email.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.SecureRandom;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.apache.axis2.java.security.TrustAllTrustManager;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

public class SslUtils {

	public static SimpleClientHttpRequestFactory getSimpleClientHttpRequestFactory() {

		SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory() {
			@Override
			protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
				if (connection instanceof HttpsURLConnection) {
					
					((HttpsURLConnection) connection).setHostnameVerifier(new HostnameVerifier() {
						public boolean verify(String hostname, SSLSession session) {
							return true;
						}
					});
					
					try {
						SSLContext sslCtx = SSLContext.getInstance("TLS");
						sslCtx.init(null, new TrustManager[] { new TrustAllTrustManager() }, new SecureRandom());
						((HttpsURLConnection) connection).setSSLSocketFactory(sslCtx.getSocketFactory());
					} catch (Exception e) {
						e.printStackTrace();
					}

					((HttpsURLConnection) connection).setAllowUserInteraction(true);
				}
				super.prepareConnection(connection, httpMethod);
			}
		};

		return clientHttpRequestFactory;
	}

}
