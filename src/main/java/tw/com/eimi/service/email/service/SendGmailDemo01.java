package tw.com.eimi.service.email.service;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendGmailDemo01 {

	// https://www.google.com/settings/security/lesssecureapps
	
	String host, port, emailid, username, password;
	Properties props = System.getProperties();
	Session l_session = null;
	
	public static void main(String [] args) {
		
		String content = "<h2><center>這是聖旨TEST</center></h2>";
		
		content += "<table border=\"1\"><tr><td>11</td><td>22</td></tr></table>";
		
		new SendGmailDemo01("rockywang101@gmail.com", "測試主旨6", content);
	}


	public SendGmailDemo01() {

		host = "smtp.gmail.com";
		port = "587";
		emailid = "rockywang101@gmail.com";
		username = "rockywang101";
		password = "xxxxx";

		emailSettings();

		createSession();

		sendMessage("rockywang101@gmail.com", "rayspeed101@gmail.com", "Test", "test Mail from gmail");
	}
	
	public SendGmailDemo01(String toEmail, String subject, String content) {
		
		host = "smtp.gmail.com";
		port = "587";
		emailid = "eimirockywang@gmail.com";
		username = "eimirockywang";
		password = "xxxxxxxxxxxxx";

		emailSettings();

		createSession();

		sendMessage(emailid, toEmail, subject, content);		
	}

	public void emailSettings() {

		props.put("mail.smtp.port", port);
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.stmp.user", emailid);
		props.put("mail.smtp.password", password);
	}

	public void createSession() {

		l_session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

//		l_session.setDebug(true); // Enable the debug mode

	}

	public boolean sendMessage(String emailFromUser, String toEmail, String subject, String msg) {
		
		try {
			// System.out.println("Sending Message
			// *********************************** ");
			MimeMessage message = new MimeMessage(l_session);
			emailid = emailFromUser;
			
			message.setFrom(new InternetAddress(emailid));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
//			message.addRecipient(Message.RecipientType.BCC, new InternetAddress(emailFromUser));
			message.setSubject(subject, "UTF-8");
			message.setContent(msg, "text/html;charset=UTF-8");

			Transport.send(message);
			System.out.println("Message Sent");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} // end catch block
		return true;
	}

}