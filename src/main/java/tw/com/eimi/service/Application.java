package tw.com.eimi.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;


/**
 * 有沒有這個，有沒有差，不確定，先留著
 * 
 * @author rocky.wang
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		
        return application.sources(Application.class);
    }

	
	public static void main(String[] args) {
	
		SpringApplication.run(Application.class, args);
	}
	
}
